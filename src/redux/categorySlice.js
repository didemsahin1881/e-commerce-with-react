import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  categories: [],
};

//create async thunk function

export const getCategories = createAsyncThunk("category", async () => {
  const response = await fetch("https://fakestoreapi.com/products/categories");
  const data = response.json();
  console.log(data, "data");
  return data;
});

//Cannot use asynchronous api with reducer so extraReducers used
const categorySlice = createSlice({
  name: "categories",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCategories.fulfilled, (state, action) => {
      console.log(action.payload);
      state.categories = action.payload;
    });
  },
});

export default categorySlice.reducer;
