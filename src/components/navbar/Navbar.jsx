import React from 'react'
import NavbarLeft from './navbarItem/NavbarLeft.jsx'
import NavbarRight from './navbarItem/NavbarRight.jsx'
import NavbarR from './navbarItem/NavbarR.jsx'

const Navbar = () => {
  return (
      <div className='flex justify-between items-center my-5'>
          
          <NavbarLeft />  
          <NavbarR/> 
          
    </div>
  )
}

export default Navbar
