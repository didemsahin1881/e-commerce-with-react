import React, { useEffect } from 'react';
import { FiSearch } from 'react-icons/fi';
import { AiOutlineHeart } from 'react-icons/ai';
import { SlBasket } from 'react-icons/sl';
import { useDispatch, useSelector } from 'react-redux';
import { getCardTotal } from '../../../redux/cardSlice';
import { useNavigate } from 'react-router-dom';


const NavbarR = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { itemCount } = useSelector(state => state.cards);
    console.log("cards", itemCount); 
    useEffect(() => {
        dispatch(getCardTotal());
    },[dispatch])
  return (
    <div className='flex items-center gap-8'>
          <div className='flex items-center border p-3 rounded-full bg-gray-100'>
              <input className='bg-gray-100 outline-none' type='text' placeholder='Arama yapınız.'></input>
              <FiSearch size={28}></FiSearch>
          </div>
          <AiOutlineHeart size={28}></AiOutlineHeart>
          <div onClick={ ()=> navigate("card")} className='relative'>
              <div className='absolute -top-3 -right-3 bg-red-500 text-white rounded-full w-5 h-5 flex items-center justify-center'>{ itemCount}</div>
              <SlBasket size={28}></SlBasket>
          </div>
    </div>
  )
}

export default NavbarR
