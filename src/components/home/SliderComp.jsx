import React from 'react'
import Slider from 'react-slick';

const SliderComp = () => {
    const settings = {
      //dots: true,
      infinite: true,
      speed: 500,
      autoplay:true,
      slidesToShow: 1,
      slidesToScroll: 1
    };
  return (
    <div>
      <Slider {...settings}>
              <div className='!flex items-center bg-gray-100 px-6'>
                  <div className=''>
                      <div className='text-6xl font-bold'>Top quality shoes are here</div>
                      <div className='text-lg my-6'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi cupiditate in architecto quaerat nihil quis et enim ad voluptatibus, doloribus odio iure molestiae? Exercitationem sint libero, sapiente placeat ipsam porro!</div>
                      <div className='border rounded-full cursor-pointer text-2xl w-[200px] h-16 flex items-center justify-center bg-gray-200'>İncele</div>
                  </div>
            <img src="https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/e777c881-5b62-4250-92a6-362967f54cca/air-force-1-07-ayakkab%C4%B1s%C4%B1-Dz225W.png" alt=""></img>
          </div>
                <div className='!flex items-center bg-gray-100 px-6'>
                   <div className=''>
                      <div className='text-6xl font-bold'>Top quality shoes are here</div>
                      <div className='text-lg my-6'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi cupiditate in architecto quaerat nihil quis et enim ad voluptatibus, doloribus odio iure molestiae? Exercitationem sint libero, sapiente placeat ipsam porro!</div>
                      <div className='border rounded-full cursor-pointer text-2xl w-[200px] h-16 flex items-center justify-center bg-gray-200'>İncele</div>
                  </div>
            <img src="https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/uirypqqxq83mwcjbf9ej/air-monarch-4-antrenman-ayakkab%C4%B1s%C4%B1-VrTWXJJn.png" alt=""></img>
          </div>
           
        </Slider>
    </div>
  )
}

export default SliderComp
