import React from 'react'
import { removeFromCard } from '../../redux/cardSlice';
import { useDispatch } from 'react-redux';

const CardComp = ({ card }) => {
    const dispatch = useDispatch();
    console.log(card);
  return (
      <div className='my-6 flex items-center justify-between'>
          <img className='w-[150px] h-[150px] my-5 object-cover' src={card?.image} alt=""></img>
          <div className='w-[476px]'>
              <div className='text-xl'>{ card?.title}</div>
              <div>{ card?.description}</div>
          </div>
          <div className='font-bold text-2xl'>{card?.price}TL - ({card?.quantity}) </div>
          <div onClick={()=> dispatch(removeFromCard(card?.id))} className='bg-red-500 text-white w-[160px] flex items-center text-2xl justify-center curspor-pointer rounded-md px-4 py-2'>ÜRÜNÜ SİL </div>
      
    </div>
  )
}

export default CardComp
