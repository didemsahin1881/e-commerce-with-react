import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { addToCard } from '../../redux/cardSlice';

const DetailComp = ({ productDetail }) => {
    const dispatch = useDispatch();
    const [quantity, setQuantity] = useState(0);
    const decrement = () => {
        if (quantity > 0) {
            setQuantity(quantity - 1);
        }
        console.log("decrement", quantity);
    }
    const increment = () => {
        if (quantity < productDetail?.rating?.count) {
            setQuantity(quantity + 1);
        } 

        console.log("increment", quantity);
    }
    const addBasket = () => {
        var BasketQuantity = quantity; 
        dispatch(addToCard({
            id: productDetail?.id,
            title: productDetail?.title,
            price: productDetail?.price,
            image: productDetail?.image,
            quantity: BasketQuantity,
        })); 
        
    }
  return (
    <div className='flex gap-10 my-10'>
          <img className='w-[500px] h-[400px] object-cover' src={productDetail?.image} alt=""></img>
          <div className=''>
              <div className='text-4xl font-bold'>{productDetail?.title}</div>
              <div className='my-2 text-2xl'>{productDetail?.description}</div>
              <div className='my2 text-xl text-red-500'>Rating: {productDetail?.rating?.rate }</div>
              <div className='my2 text-xl text-red-500'>Count: {productDetail?.rating?.count }</div>
              <div className='text-3xl font-bold'>Price: {productDetail?.price } <span className='text-3xl'></span></div>
          
              <div className='flex items-center gap-5 my-4'>
                  <div onClick={decrement} className='text-5xl cursor-pointer'>
                      -
                  </div>
                  <input readOnly={true} className='w-12 text-center text-4xl font-bold' type='text' value={quantity}></input>
                  <div onClick={increment} className='text-4xl cursor-pointer'>+</div>
              </div>
              <div onClick={addBasket} className='border w-[200px] text-2xl rounded-md bg-gray-200 cursor-pointer my-4 h-16 flex items-center justify-center'>Sepete Ekle</div>
          </div>
    </div>
  )
}

export default DetailComp
