import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getCardTotal } from '../redux/cardSlice';
import CardComp from '../components/card/CardComp';

const Card = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { cards, totalAmount, itemCount } = useSelector(state => state.cards);
    console.log("totalAmount", totalAmount);
    console.log("itemCount", itemCount);
    console.log("cards", cards); 
    useEffect(() => {
        dispatch(getCardTotal());
    },[dispatch])
  return (
    <div>
          {
              cards?.length > 0 ? <div>
                  {
                      cards?.map((card, i) => (
                           <CardComp key={i} card={card}></CardComp>
                      ))
                  }
                  <div className="flex flex-items justify-end font-bold text-2xl border-t-[1px]">TOPLAM TUTAR : <span className='font-bold text-3xl'> { totalAmount}</span></div>
              </div> :
                  <div>Sepetiniz Boş !</div>
      }
    </div>
  )
}

export default Card
